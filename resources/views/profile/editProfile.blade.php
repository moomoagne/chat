@extends('layouts.app')

@section('content')
    <div class="container">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('/home')}}">Accueil</a></li>
        <li class="breadcrumb-item"><a href="{{url('/profile')}}/{{Auth::user()->slug}}">Profile</a></li>
        <li class="breadcrumb-item"><a href="">Edit Profile</a></li>
    </ol>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="col-lg-8 offset-4 col-sm-12 col-md-12">
                        <div class="thumbnail">
                            <div class="row" align="center"><h3 >Edition profile de: {{ucwords(Auth::user()->name)}}</h3></div>
                            <div class="row" align="center">
                                <img src="{{url('../')}}/uploads/images/{{Auth::user()->photo}}" width="120px" height="120px" class="rounded-circle"/>
                                <div class="caption ml-4">
                                    <p align="center">{{$data->city}} - {{$data->country}} </p>
                                    <p>  <a href="{{url('/')}}/changePhoto"  class="btn btn-primary" role="button">Change Image</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="col-lg-12 offset-2 col-sm-12 col-md-12">

                        <form action="{{url('/updateProfile')}}" method="post">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                            <div class="col-md-8">

                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Votre ville" name="city" value="{{$data->city}}">
                                </div>
                                <br>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Votre Pays" name="country" value="{{$data->country}}">
                                </div>


                            </div>

                            <div class="col-md-8"><br>
                                <div class="input-group">
                                    <textarea type="text" placeholder="Votre description" class="form-control" name="about">{{$data->about}}</textarea>
                                </div>

                                <br>

                                <div class="input-group">

                                    <input type="submit" value="Modifier" class="btn btn-success pull-right" >
                                </div>
                            </div>

                        </form>



                    </div>


                </div>
            </div>
        </div>
    </div>
    </div>
@endsection