@extends('layouts.app')

@section('content')
    <div class="container">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{url('/profile')}}/{{Auth::user()->slug}}">Profile</a></li>
        <li class="breadcrumb-item"><a href="{{url('/editProfile')}}">Edit Profile</a></li>
        <li class="breadcrumb-item"><a href="">Change Image</a></li>
    </ol>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading" align="center"><h3>Changer votre image de profiles</h3></div>
            <div class="panel-body">

                <div class="col-md-6 offset-3">

                    <img style="margin-left: 40%;" class="rounded-circle" src="{{url('../')}}/uploads/images/{{Auth::user()->photo}}" width="100px" height="100px"/><br>
                    <br>
                    <hr>
                    <br>
                    <form action="{{url('/')}}/uploadPhoto" method="post" enctype="multipart/form-data">

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <input type="file" name="photo" class="form-control"/>
                        <br>
                        <input type="submit" class="btn btn-success" name="btn"/>

                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection