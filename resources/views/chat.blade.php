
<style>
    .list-group{
        overflow-y: scroll;
        height: 200px;
    }
</style>
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row" id="app">
        <div class="offset-4 col-4 offset-sm-1 col-sm-10">
            <li class="list-group-item active">Live Chat<span class="badge badge-pill badge-danger">@{{ numberOfUsers  }}</span></li>
            <div class="badge badge-pill badge-primary">@{{ typing }}</div>
            <ul class="list-group" v-chat-scroll>
                 <message v-for="value,index in chat.message"
                          :key=value.index
                          :color= chat.color[index]
                          :time= chat.time[index]
                          :user = chat.user[index]>@{{ value }}</message>
                <input type="text" class="form-control" placeholder="Tapez votre message ici ..."
                       v-model="message" @keyup.enter="send"><br>
                <span><a href="" class="btn btn-danger btn-sm" @click.privent="deleteSession">Supprimer</a></span>
            </ul>

        </div>
    </div>
</div>
@endsection
