<?php

namespace App\Http\Controllers;

use App\Article;
use function back;
use function compact;
use function dump;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;
use function redirect;
use function var_dump;
use function view;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{
    public function index ()
    {
        $articles = Article::all();
        return view('article.index',['articles' => $articles]);
    }


    public function createArticle(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'contenu' => 'required'
        ]);
        $name = $request->name;
        $contenu = $request->contenu;
        DB::table('articles')
            ->insert(['name' => $name, 'contenu' => $contenu
            ,'user_id' => Auth::user()->id]);
       // dump($createArticles);die();
        return redirect('/articles')->with('info','Article enregistrer');
    }


    public function show ($id)
    {
        $article = Article::find($id);
        return view('article.show', ['article'=> $article]);
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update ($id){
        $articles = Article::find($id);
        return view('article.update', ['articles' => $articles]);
    }

    public function edit (Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'contenu' => 'required'
        ]);
        $name = $request->name;
        $contenu = $request->contenu;
        Article::where('id', $id)
            ->update(['name'=>$name, 'contenu'=>$contenu,'user_id'=>Auth::user()->id]);
        return redirect('/articles')->with('info', 'Article mis a jours');
    }

    public function delete($id)
    {
        Article::where('id',$id)
            ->delete();
        return redirect('/articles')->with('info', 'Article supprimer');
    }

}
