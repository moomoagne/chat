<?php
/**
 * Created by PhpStorm.
 * User: mamadou
 * Date: 12/19/17
 * Time: 11:15 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class profile extends Model
{
    protected $fillable =['city', 'country', 'about', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}