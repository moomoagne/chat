<?php
/**
 * Created by PhpStorm.
 * User: mamadou
 * Date: 12/19/17
 * Time: 11:15 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable =['name', 'contenu'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}