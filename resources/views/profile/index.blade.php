@extends('layouts.app')

@section('content')
    <div class="container">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('/home')}}">Accueil</a></li>
        <li class="breadcrumb-item"><a href="{{url('/profile')}}/{{Auth::user()->slug}}">Profile</a></li>
    </ol>
    <div class="row">
        @foreach($userData as $uData)
            @if ($uData->user_id == Auth::user()->id)
                <div class="col-md-12">
                    <div class="panel">
                        {{--<div class="panel-heading "><h1>{{$uData->name}}</h1></div>--}}

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <div class="thumbnail">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <h3 align="center">{{$uData->name}}</h3>
                                                <img style="margin-left: 60px;" align="center" src="{{url('../')}}/uploads/images/{{$uData->photo}}" width="120px" height="120px" class="rounded-circle"/>
                                            </div>
                                            <div class="col-sm-6"><br>
                                                <div class="thumbnail">
                                                    <div class="caption">
                                                        <span align="center">Email: {{$uData->email}}</span><br>
                                                        <span align="center">Sexe: {{$uData->gender}}</span><br>
                                                        <span align="center">Ville/Pays :{{$uData->city}} - {{$uData->country}}</span><br><br>
                                                        <span align="center"><a href="{{url('/editProfile')}}" class="btn btn-primary" role="button">Modifier Profile</a></span>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6">
                                    <h4 class=""><span class="label label-default">A Propos</span></h4>
                                    <p> {{$uData->about}} </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        @endforeach
    </div>
    </div>
@endsection