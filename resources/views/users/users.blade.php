@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-3">
            <div class="panel panel-default">
                    <div class="row">
                        @foreach($users as $user)
                            <div class="col-sm-3">
                                <h4 align="center">{{$user->name}}</h4>
                                <a  href="{{url('/profile')}}/{{$user->slug}}">
                                    <img style="margin-left: 60px;" align="center" src="{{url('../')}}/uploads/images/{{$user->photo}}" width="120px" height="120px" class="rounded-circle"/>
                                </a>
                            </div>
                        @endforeach
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
