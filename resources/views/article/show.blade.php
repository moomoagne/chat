@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel panel-heading">
                        Artilce
                    </div>
                    <div class="panel panel-body">
                        <p>
                            <span>Name : {{$article->name}}</span>
                            <span>Contenu : {{$article->contenu}}</span>
                        </p>
                        <a href='{{ url("/articles/delete/{$article->id}") }}' class="btn pill btn-danger">Delete</a>
                        <a href='{{ url("/articles/update/{$article->id}") }}' class="btn pill btn-primary">Update</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
