@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>Liste des articles</h3><a href="{{url('/articles/new')}}">creer article</a></div>
                    <div class="panel panel-body">
                        @if (count($articles) >0)
                            @if(session('info'))
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="alert alert-success">
                                            {{session('info')}}
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <table>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Contenu</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($articles as $article)
                                        <tr>
                                            <td>{{$article->id}}</td>
                                            <td>{{$article->name}}</td>
                                            <td>{{$article->contenu}}</td>
                                            <td>
                                            <a href='{{ url("/articles/delete/{$article->id}") }}' class="btn pill btn-danger">Delete</a>
                                            <a href='{{ url("/articles/show/{$article->id}") }}' class="btn pill btn-warning">Show</a>
                                            <a href='{{ url("/articles/update/{$article->id}") }}' class="btn pill btn-primary">Update</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @else
                        <h4>Pas d'articles</h4>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
