<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Http\Controllers\ArticleController;

Route::get('/', function () {
    return view('welcome');
});

Route::get('checkEmail', function(){
    return new App\Mail\Email();
});
Route::group(['middleware' => 'auth'], function (){
    Route::get('chat','ChatController@chat');
    Route::post('send','ChatController@send');
    Route::post('saveToSession','ChatController@saveToSession');
    Route::post('deleteSession','ChatController@deleteSession');
    Route::post('getOldMessage','ChatController@getOldMessage');

    Route::get('/users', 'profileController@getAllUser');
    Route::get('/profile/{slug}', 'profileController@index');
    Route::get('/editProfile', 'profileController@editProfileForm');
    Route::post('/uploadPhoto','profileController@uploadPhoto');
    Route::post('/updateProfile','profileController@updateProfile');
    Route::get('/changePhoto', function (){
        return view('profile.photo');
    });

    Route::get('/articles', 'ArticleController@index');
    Route::get('/articles/new', function (){
        return view('article.new');
    });
    Route::get('/articles/show/{id}', 'ArticleController@show');
    Route::get('/articles/update/{id}', 'ArticleController@update');
    Route::post('/articles/edit/{id}', 'ArticleController@edit');
    Route::get('/articles/delete/{id}', 'ArticleController@delete');
    Route::post('/createArticle', 'ArticleController@createArticle');
});



Route::get('check', function (){
   return session('chat');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
